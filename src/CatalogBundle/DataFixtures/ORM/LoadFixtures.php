<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;

class LoadFixtures implements FixtureInterface
{
    private $supplierImages = [
        'imagenProveedor1.jpg',
        'imagenProveedor2.jpg',
        'imagenProveedor3.jpg',
        'imagenProveedor4.jpg',
        'imagenProveedor5.jpg'
    ];

    private $productImages = [
        'imagenProducto1.jpg',
        'imagenProducto2.jpg',
        'imagenProducto3.jpg',
        'imagenProducto4.jpg',
        'imagenProducto5.jpg'
    ];

    private $productNames = [
        'Dron Helmio',
        'Dron Ambers',
        'Super Dron XD5',
        'Dron Superb',
        'Dron JR-5'
    ];

    public function load(ObjectManager $manager)
    {
        Fixtures::load(__DIR__ . '/fixtures.yml',
            $manager,
            [
                'providers' => [$this]
            ]);
    }

    public function imagesSupplier()
    {
        return array_pop($this->supplierImages);
    }

    public function imagesProducts()
    {
        return array_pop($this->productImages);
    }

    public function productsName()
    {
        return array_pop($this->productNames);
    }
}