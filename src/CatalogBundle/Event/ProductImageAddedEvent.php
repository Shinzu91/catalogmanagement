<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Event;

use CatalogBundle\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ProductImageAddedEvent, an extension of Event
 */
class ProductImageAddedEvent extends Event
{
    /**
     * @var Product
     */
    private $product;

    /**
     * ProductImageAddedEvent constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}