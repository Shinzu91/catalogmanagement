<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Event;

use CatalogBundle\Entity\Supplier;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class SupplierImageDeletedEvent, an extension of Event
 */
class SupplierImageDeletedEvent extends Event
{
    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * SupplierImageDeletedEvent constructor.
     *
     * @param Supplier $supplier
     */
    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Supplier
     */
    public function getSupplierDeleted()
    {
        return $this->supplier;
    }
}