<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Event;

use CatalogBundle\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ProductImageDeletedEvent, an extension of Event
 */
class ProductImageDeletedEvent extends Event
{
    /**
     * @var Product
     */
    private $product;

    /**
     * ProductImageDeletedEvent constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProductDeleted()
    {
        return $this->product;
    }
}