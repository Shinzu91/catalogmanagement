<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use CatalogBundle\Entity\Supplier;

/**
 * Class SupplierImageAddedEvent, an extension of Event
 */
class SupplierImageAddedEvent extends Event
{
    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * SupplierImageAddedEvent constructor.
     *
     * @param Supplier $supplier
     */
    public function __construct (Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }
}