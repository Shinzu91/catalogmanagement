<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Class SupplierType
 */
class SupplierType extends AbstractType
{
    /**
     * Build the form of supplier
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array ('label' => 'Nombre'))
            ->add('street', TextType::class, array ('label' => 'Calle'))
            ->add('number', IntegerType::class, array ('label' => 'Número'))
            ->add('city', TextType::class, array ('label' => 'Ciudad'))
            ->add('state', TextType::class, array ('label' => 'Estado'))
            ->add('zipCode', TextType::class, array ('label' => 'Código Postal'))
            ->add('country', TextType::class, array ('label' => 'Población'))
            ->add('CIF', TextType::class, array (
                'label' => 'CIF',
                'constraints' => new Length([
                    'min' => '9',
                    'max' => '9'
                ])
            ))
            ->add('logo', FileType::class, array (
                'label' => 'Subir imagen jpeg',
                'data_class' => null,
                'constraints' => new File([
                    'maxSize' => '1000000'
                ])
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array (
            'data_class' => 'CatalogBundle\Model\SupplierModel'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'supplierModel';
    }
}