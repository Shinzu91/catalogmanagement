<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Class ProductType, an extension of AbstractType
 */
class ProductType extends AbstractType
{
    /**
     * Build the form of product
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array (
                'label' => 'Nombre',
                'constraints' => new Length([
                    'min' => 5,
                    'max' => 20
                ])
            ))
            ->add('description', TextType::class, array (
                'label' => 'Descripción',
                'constraints' => new Length([
                    'min' => 20,
                    'max' => 200
                ])
            ))
            ->add('price', IntegerType::class, array (
                'label' => 'Precio',
                'constraints' => new Range([
                    'min' => 1,
                    'max' => 6000
                ])
            ))
            ->add('image', FileType::class, array (
                'label' => 'Subir imagen en formato jpeg',
                'data_class' => null,
                'constraints' => new File([
                    'maxSize' => '1000000'
                ])
            ))
            ->add('supplier', EntityType::class, array (
                'class' => 'CatalogBundle\Entity\Supplier',
                'choice_label' => 'name',
                'expanded' => false,
                'label' => 'Proveedor'
            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array (
            'data_class' => 'CatalogBundle\Model\ProductModel'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'productModel';
    }
}