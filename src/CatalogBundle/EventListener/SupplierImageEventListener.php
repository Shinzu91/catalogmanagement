<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\EventListener;

use CatalogBundle\Service\Supplier\UpdateSupplier;

/**
 * Class SupplierImageEventListener
 */
class SupplierImageEventListener
{

    /**
     * @var UpdateSupplier
     */
    private $updatedSupplier;

    /**
     * SupplierImageEventListener constructor.
     *
     * @param UpdateSupplier $updatedSupplier
     */
    public function __construct(UpdateSupplier $updatedSupplier)
    {
        $this->updatedSupplier = $updatedSupplier;
    }

    /**
     * @param $event
     */
    public function updateSupplierImage($event)
    {
        $this
            ->updatedSupplier
            ->update(
                $event->getUpdatedSupplier());
    }
}
