<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\EventListener;

use CatalogBundle\Service\Product\UpdateProduct;

/**
 * Class ProductImageEventListener
 */
class ProductImageEventListener
{

    /**
     * @var UpdateProduct
     */
    private $updatedSupplier;

    /**
     * ProductImageEventListener constructor.
     *
     * @param UpdateProduct $updatedSupplier
     */
    public function __construct(UpdateProduct $updatedSupplier)
    {
        $this->updatedSupplier = $updatedSupplier;
    }

    /**
     * @param $event
     */
    public function updateProductImage($event)
    {
        $this
            ->updatedSupplier
            ->update(
                $event->getUpdatedProduct());
    }
}
