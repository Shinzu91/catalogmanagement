<?php

/**
 * CatalogBundle for Symfony3
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * CatalogBundle, an extension of Bundle.
 */
class CatalogBundle extends Bundle
{
}