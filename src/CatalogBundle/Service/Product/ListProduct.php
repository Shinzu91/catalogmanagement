<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Product;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ListProduct
 */
class ListProduct
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ListProduct constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Shows a list of all products
     *
     * @return array|\CatalogBundle\Entity\Product[]
     */
    public function listProducts()
    {
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Product');
        $products                = $entityManagerRepository->findAll();

        return $products;
    }
}