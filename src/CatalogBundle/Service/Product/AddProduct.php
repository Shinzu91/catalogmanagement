<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Product;

use CatalogBundle\Entity\Product;
use CatalogBundle\Event\ProductImageAddedEvent;
use CatalogBundle\Form\Type\ProductType;
use CatalogBundle\Model\ProductModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AddProduct
 */
class AddProduct
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FormFactoryInterface
     */
    private $form;

    /**
     * @var TraceableEventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * AddProduct constructor.
     *
     * @param EntityManagerInterface            $entityManager
     * @param FormFactoryInterface              $form
     * @param TraceableEventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form, TraceableEventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager   = $entityManager;
        $this->form            = $form;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * Render a form and show it, after that, saves the product.
     *
     * @param Request $request
     *
     * @return array|null
     */
    public function add(Request $request)
    {
        $productModel            = new ProductModel();
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Product');

        $form = $this->form->create(ProductType::class, $productModel);
        $form->add('submit', SubmitType::class, array (
            'label' => 'Crear',
            'attr' => array ('class' => 'btn btn-default pull-right')
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $product = $this->transformModelToProduct($form->getData());
            $this->eventDispatcher->dispatch('product_image_upload', new ProductImageAddedEvent($product));
            return null;
        }

        return array ('form' => $form->createView());
    }

    /**
     * Transform a model object to a product
     *
     * @param ProductModel $productModel
     *
     * @return Product
     */
    private function transformModelToProduct(ProductModel $productModel)
    {
        $product = new Product();

        $product->setName($productModel->getName());
        $product->setDescription($productModel->getDescription());
        $product->setPrice($productModel->getPrice());
        $product->setImage($productModel->getImage());
        $product->setSupplier($productModel->getSupplier());

        return $product;
    }

}