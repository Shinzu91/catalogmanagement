<?php


namespace CatalogBundle\Service\Product;

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

use CatalogBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UpdateProduct
 */
class UpdateProduct
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UpdateProduct constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Persist and flush the product
     *
     * @param Product $product
     */
    public function update(Product $product)
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }
}

