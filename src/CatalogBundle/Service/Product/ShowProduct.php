<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Product;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ShowProduct
 */
class ShowProduct
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ShowProduct constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Shows an existing product with the id
     *
     * @param int $id
     *
     * @return \CatalogBundle\Entity\Product
     */
    public function show(int $id)
    {
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Product');
        $product                 = $entityManagerRepository->find($id);

        return $product;
    }
}