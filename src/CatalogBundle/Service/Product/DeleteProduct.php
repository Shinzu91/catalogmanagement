<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Product;

use CatalogBundle\Event\ProductImageDeletedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;

/**
 * Class DeleteProduct
 */
class DeleteProduct
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TraceableEventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * DeleteProduct constructor.
     *
     * @param EntityManagerInterface            $entityManager
     * @param TraceableEventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, TraceableEventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager   = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Deletes a product with the id
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Product');
        $product               = $entityManagerRepository->find($id);

        $this->eventDispatcher->dispatch('product_image_delete', new ProductImageDeletedEvent($product));
        $this->entityManager->remove($product);
        $this->entityManager->flush();

    }
}