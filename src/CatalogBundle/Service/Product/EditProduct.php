<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Product;

use CatalogBundle\Entity\Product;
use CatalogBundle\Event\ProductImageAddedEvent;
use CatalogBundle\Event\ProductImageDeletedEvent;
use CatalogBundle\Form\Type\ProductType;
use CatalogBundle\Model\ProductModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class EditProduct
 */
class EditProduct
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FormFactoryInterface
     */
    private $form;

    /**
     * @var TraceableEventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     *
     * EditProduct constructor.
     *
     * @param EntityManagerInterface            $entityManager
     * @param FormFactoryInterface              $form
     * @param TraceableEventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form, TraceableEventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager   = $entityManager;
        $this->form            = $form;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * Edits an existing product, rendering the form.
     *
     * @param int     $id
     * @param Request $request
     *
     * @return array|null
     */
    public function edit(int $id, Request $request)
    {
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Product');
        $product                 = $entityManagerRepository->find($id);
        $arrayProduct            = $this->transformProductToModel($product);

        $form = $this->form->create(ProductType::class, $arrayProduct);
        $form->add('submit', SubmitType::class, array (
            'label' => 'Editar',
            'attr' => array ('class' => 'btn btn-default pull-right')
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->eventDispatcher->dispatch('product_image_delete', new ProductImageDeletedEvent($product));
            $productUpdated = $this->updateProduct($form->getData(), $product);
            $this->eventDispatcher->dispatch('product_image_upload', new ProductImageAddedEvent($productUpdated));
            return null;
        }

        return array ('form' => $form->createView());
    }

    /**
     *
     * Transform a product object to a model
     *
     * @param Product $product
     *
     * @return ProductModel
     */
    private function transformProductToModel(Product $product)
    {
        $productModel = new ProductModel();

        $productModel->setName($product->getName());
        $productModel->setDescription($product->getDescription());
        $productModel->setPrice($product->getPrice());
        $productModel->setImage($product->getImage());
        $productModel->setSupplier($product->getSupplier());

        return $productModel;
    }

    /**
     *
     * Transform a model object to a product
     *
     * @param ProductModel $productModel
     * @param Product      $product
     *
     * @return Product
     */
    private function updateProduct(ProductModel $productModel, Product $product)
    {
        $product->setName($productModel->getName());
        $product->setDescription($productModel->getDescription());
        $product->setPrice($productModel->getPrice());
        $product->setImage($productModel->getImage());
        $product->setSupplier($productModel->getSupplier());

        return $product;
    }

}