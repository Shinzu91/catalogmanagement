<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Supplier;

use CatalogBundle\Entity\Supplier;
use CatalogBundle\Event\SupplierImageAddedEvent;
use CatalogBundle\Event\SupplierImageDeletedEvent;
use CatalogBundle\Form\Type\SupplierType;
use CatalogBundle\Model\SupplierModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class EditSupplier
 */
class EditSupplier
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * @var FormFactoryInterface
     */
    private $form;


    /**
     * @var TraceableEventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * EditSupplier constructor.
     *
     * @param EntityManagerInterface            $entityManager
     * @param FormFactoryInterface              $form
     * @param TraceableEventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form, TraceableEventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager   = $entityManager;
        $this->form            = $form;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * Edits an existing supplier, rendering the form.
     *
     * @param int     $id
     * @param Request $request
     *
     * @return array|null
     */
    public function edit(int $id, Request $request)
    {
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Supplier');
        $supplier                = $entityManagerRepository->find($id);
        $supplierModel           = $this->transformSupplierToModel($supplier);

        $form = $this->form->create(SupplierType::class, $supplierModel);
        $form->add('submit', SubmitType::class, array (
            'label' => 'Editar',
            'attr' => array ('class' => 'btn btn-default pull-right')
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->eventDispatcher->dispatch('supplier_image_delete', new SupplierImageDeletedEvent($supplier));
            $supplierUpdated = $this->updateSupplier($form->getData(), $supplier);
            $this->eventDispatcher->dispatch('supplier_image_upload', new SupplierImageAddedEvent($supplierUpdated));
            return null;
        }

        return array ('form' => $form->createView());
    }

    /**
     * Transforms a supplier object to a model
     *
     * @param Supplier $supplier
     *
     * @return SupplierModel
     */
    private function transformSupplierToModel(Supplier $supplier)
    {
        $supplierModel = new SupplierModel();

        $supplierModel->setName($supplier->getName());
        $supplierModel->setStreet($supplier->getStreet());
        $supplierModel->setNumber($supplier->getNumber());
        $supplierModel->setCity($supplier->getCity());
        $supplierModel->setState($supplier->getState());
        $supplierModel->setZipCode($supplier->getZipCode());
        $supplierModel->setCountry($supplier->getCountry());
        $supplierModel->setCIF($supplier->getCIF());
        $supplierModel->setLogo($supplier->getLogo());

        return $supplierModel;
    }

    /**
     * Update supplier parameters
     *
     * @param SupplierModel $supplierModel
     * @param Supplier      $supplier
     *
     * @return Supplier
     */
    private function updateSupplier(SupplierModel $supplierModel, Supplier $supplier)
    {
        $supplier->setName($supplierModel->getName());
        $supplier->setStreet($supplierModel->getStreet());
        $supplier->setNumber($supplierModel->getNumber());
        $supplier->setCity($supplierModel->getCity());
        $supplier->setState($supplierModel->getState());
        $supplier->setZipCode($supplierModel->getZipCode());
        $supplier->setCountry($supplierModel->getCountry());
        $supplier->setCIF($supplierModel->getCIF());
        $supplier->setLogo($supplierModel->getLogo());

        return $supplier;
    }

}