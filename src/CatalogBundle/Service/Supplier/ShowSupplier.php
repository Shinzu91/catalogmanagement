<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Supplier;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ShowSupplier
 *
 * @package CatalogBundle\Service\Supplier
 */
class ShowSupplier
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ShowSupplier constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Shows an existing supplier with the id
     *
     * @param int $id
     *
     * @return \CatalogBundle\Entity\Supplier
     */
    public function show(int $id)
    {
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Supplier');
        $supplier                = $entityManagerRepository->find($id);

        return $supplier;
    }
}