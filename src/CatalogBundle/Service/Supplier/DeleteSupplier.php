<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Supplier;

use CatalogBundle\Event\SupplierImageDeletedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;

/**
 * Class DeleteSupplier
 */
class DeleteSupplier
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * @var TraceableEventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * DeleteSupplier constructor.
     *
     * @param EntityManagerInterface            $entityManager
     * @param TraceableEventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, TraceableEventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager   = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * Deletes a supplier with the id
     *
     * @param $id
     */
    public function delete($id)
    {
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Supplier');
        $supplier              = $entityManagerRepository->find($id);

        $this->eventDispatcher->dispatch('supplier_image_delete', new SupplierImageDeletedEvent($supplier));
        $this->entityManager->remove($supplier);
        $this->entityManager->flush();

    }
}