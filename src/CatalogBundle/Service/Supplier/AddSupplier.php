<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Supplier;

use CatalogBundle\Entity\Supplier;
use CatalogBundle\Event\SupplierImageAddedEvent;
use CatalogBundle\Form\Type\SupplierType;
use CatalogBundle\Model\SupplierModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class AddSupplier
 */
class AddSupplier
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FormFactoryInterface
     */
    private $form;

    /**
     * @var TraceableEventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * AddSupplier constructor.
     *
     * @param EntityManagerInterface            $entityManager
     * @param FormFactoryInterface              $form
     * @param TraceableEventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form, TraceableEventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager   = $entityManager;
        $this->form            = $form;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Render a form and show it, after that, saves the supplier.
     *
     * @param $request
     *
     * @return array|null
     */
    public function add($request)
    {
        $supplierModel           = new SupplierModel();
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Supplier');

        $form = $this->form->create(SupplierType::class, $supplierModel);
        $form->add('submit', SubmitType::class, array (
            'label' => 'Crear',
            'attr' => array ('class' => 'btn btn-default pull-right')
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $supplier = $this->transformModelToSupplier($form->getData());
            $this->eventDispatcher->dispatch('supplier_image_upload', new SupplierImageAddedEvent($supplier));
            return null;
        }

        return array ('form' => $form->createView());
    }

    /**
     * Transform a model object to a supplier
     *
     * @param SupplierModel $supplierModel
     *
     * @return Supplier
     */
    private function transformModelToSupplier(SupplierModel $supplierModel)
    {
        $supplier = new Supplier();

        $supplier->setName($supplierModel->getName());
        $supplier->setStreet($supplierModel->getStreet());
        $supplier->setNumber($supplierModel->getNumber());
        $supplier->setCity($supplierModel->getCity());
        $supplier->setState($supplierModel->getState());
        $supplier->setZipCode($supplierModel->getZipCode());
        $supplier->setCountry($supplierModel->getCountry());
        $supplier->setCIF($supplierModel->getCIF());
        $supplier->setLogo($supplierModel->getLogo());

        return $supplier;
    }
}