<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Supplier;

use CatalogBundle\Entity\Supplier;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UpdateSupplier
 *
 * @package CatalogBundle\Service\Supplier
 */
class UpdateSupplier
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UpdateSupplier constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Persist and flush the supplier
     *
     * @param Supplier $supplier
     */
    public function update(Supplier $supplier)
    {
        $this->entityManager->persist($supplier);
        $this->entityManager->flush();
    }

}