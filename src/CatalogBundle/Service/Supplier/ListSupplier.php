<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Service\Supplier;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ListSupplier
 */
class ListSupplier
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ListSupplier constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Shows a list of all suppliers
     *
     * @return array|\CatalogBundle\Entity\Supplier[]
     */
    public function listSuppliers()
    {
        $entityManagerRepository = $this->entityManager->getRepository('CatalogBundle\Entity\Supplier');
        $suppliers               = $entityManagerRepository->findAll();

        return $suppliers;
    }
}