<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductController, an extension of Controller.
 */
class ProductController extends Controller
{
    /**
     *
     * Add a new product
     *
     * @Route("/product/new", name="new_product")
     * @Template
     *
     * @param Request $request
     *
     * @return array|null|RedirectResponse
     */
    public function addProductAction(Request $request)
    {
        $addProductService = $this->get('CatalogBundle.add_product');
        $addProduct        = $addProductService->add($request);

        if ($addProduct === null) {
            $this->addFlash('success', 'El producto se ha añadido correctamente.');
            return $this->redirect($this->generateUrl('list_products'));
        }
        return $addProduct;
    }

    /**
     *
     * Edit an existing product through the id
     *
     * @Route("/product/edit/{id}", name="edit_product")
     * @Template
     *
     * @param Request $request
     * @param int     $id
     *
     * @return array|null|RedirectResponse
     */
    public function editProductAction(Request $request, int $id)
    {
        $editProductService = $this->get('CatalogBundle.edit_product');
        $editProduct        = $editProductService->edit($id, $request);

        if ($editProduct == null) {
            $this->addFlash('success', 'El producto se ha editado correctamente.');
            return $this->redirect($this->generateUrl('list_products'));
        }
        return $editProduct;

    }

    /**
     *
     * Delete a product through the id
     *
     * @Route("/product/delete/{id}", name="delete_product")
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function deleteProductAction(int $id)
    {
        $deleteService = $this->get('CatalogBundle.delete_product');
        $deleteService->delete($id);
        $this->addFlash('success', 'El producto se ha eliminado correctamente.');
        return $this->redirect($this->generateUrl('list_products'));
    }

    /**
     *
     * Show a list of existing products
     *
     * @Route("/products", name="list_products")
     * @Template
     *
     * @return array
     */
    public function listProductAction()
    {
        $listProductsService = $this->get('CatalogBundle.list_product');
        $products            = $listProductsService->listProducts();
        return array ('products' => $products);
    }

    /**
     *
     * Show a product through the id
     *
     * @Route("/product/{id}", name="product")
     * @Template
     *
     * @param int $id
     *
     * @return array
     */
    public function showProductAction(int $id)
    {
        $showService = $this->get('CatalogBundle.show_product');
        $product     = $showService->show($id);
        return array ('product' => $product);
    }

}