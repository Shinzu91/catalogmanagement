<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SupplierController, an extension of Controller.
 */
class SupplierController extends Controller
{
    /**
     *
     * Add a new supplier
     *
     * @Route("/supplier/new", name="new_supplier")
     * @Template
     *
     * @param Request $request
     *
     * @return array|null|RedirectResponse
     */
    public function addSupplierAction(Request $request)
    {
        $addSupplierService = $this->get('CatalogBundle.add_supplier');
        $addSupplier        = $addSupplierService->add($request);

        if ($addSupplier == null) {
            $this->addFlash('success', 'El proveedor se ha añadido correctamente.');
            return $this->redirect($this->generateUrl('list_suppliers'));
        }

        return $addSupplier;
    }

    /**
     *
     * Edit an existing supplier through the id
     *
     * @Route("/supplier/edit/{id}", name="edit_supplier")
     * @Template
     *
     * @param Request $request
     * @param int     $id
     *
     * @return array|null|RedirectResponse
     */
    public function editSupplierAction(Request $request, int $id)
    {
        $editSupplierService = $this->get('CatalogBundle.edit_supplier');
        $editSupplier        = $editSupplierService->edit($id, $request);

        if ($editSupplier == null) {
            $this->addFlash('success', 'El proveedor se ha editado correctamente.');
            return $this->redirect($this->generateUrl('list_suppliers'));
        }
        return $editSupplier;
    }

    /**
     *
     * Delete an existing supplier through the id
     *
     * @Route("/supplier/delete/{id}", name="delete_supplier")
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function deleteSupplierAction(int $id)
    {
        $deleteService = $this->get('CatalogBundle.delete_supplier');
        $deleteService->delete($id);
        $this->addFlash('success', 'El proveedor se ha eliminado correctamente.');
        return $this->redirect($this->generateUrl('list_suppliers'));
    }

    /**
     *
     * Show a list of existing products
     *
     * @Route("/suppliers", name="list_suppliers")
     * @Template
     *
     * @return array
     */
    public function listSupplierAction()
    {
        $listProductsService = $this->get('CatalogBundle.list_supplier');
        $suppliers           = $listProductsService->listSuppliers();
        return array ('suppliers' => $suppliers);
    }

    /**
     *
     * Show a product through the id
     *
     * @Route("/supplier/{id}", name="supplier")
     * @Template
     *
     * @param int $id
     *
     * @return array
     */
    public function showSupplierAction(int $id)
    {
        $showService = $this->get('CatalogBundle.show_supplier');
        $supplier    = $showService->show($id);
        return array ('supplier' => $supplier);
    }

}