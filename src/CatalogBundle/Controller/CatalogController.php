<?php

/**
 * This file is part of the CatalogBundle for Symfony3.
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class CatalogController, an extension of Controller.
 */
class CatalogController extends Controller
{
    /**
     * @Route("/", name="initial_catalog")
     * @Template
     */
    public function showAction()
    {
        $listProductsService = $this->get('CatalogBundle.list_product');
        $products            = $listProductsService->listProducts();
        $listProductsService = $this->get('CatalogBundle.list_supplier');
        $suppliers           = $listProductsService->listSuppliers();
        
        return array ('products' => $products, 'suppliers' => $suppliers);
    }


}