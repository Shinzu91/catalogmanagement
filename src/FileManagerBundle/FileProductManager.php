<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle;

use CatalogBundle\Entity\Product;
use FileManagerBundle\Event\ImageProductUploadedEvent;
use FileManagerBundle\Services\FileDeleter;
use FileManagerBundle\Services\FileUploader;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;

/**
 * Class FileProductManager
 */
class FileProductManager
{

    /**
     * @var FileDeleter
     */
    private $fileDeleter;

    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @var TraceableEventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * FileProductManager constructor.
     *
     * @param FileDeleter                       $fileDeleter
     * @param FileUploader                      $fileUploader
     * @param TraceableEventDispatcherInterface $eventDispatcher
     */
    public function __construct(FileDeleter $fileDeleter, FileUploader $fileUploader, TraceableEventDispatcherInterface $eventDispatcher)
    {
        $this->fileDeleter     = $fileDeleter;
        $this->fileUploader    = $fileUploader;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Deletes a product image from the image directory
     *
     * @param Product $product
     */
    public function deleteProductImage(Product $product)
    {
        $this->fileDeleter->delete($product->getImage());
    }

    /**
     * Adds a product image to the image directory and
     * dispatches a new event with the new name of the image
     *
     * @param Product $product
     */
    public function addProductImage(Product $product)
    {
        $imageUploaded = $this->fileUploader->upload($product->getImage());
        $product->setImage($imageUploaded);
        $this->eventDispatcher->dispatch('product_image_uploaded', new ImageProductUploadedEvent($product));
    }
}