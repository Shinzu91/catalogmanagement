<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle;

use CatalogBundle\Entity\Supplier;
use FileManagerBundle\Event\ImageSupplierUploadedEvent;
use FileManagerBundle\Services\FileDeleter;
use FileManagerBundle\Services\FileUploader;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;

/**
 * Class FileSupplierManager
 */
class FileSupplierManager
{

    /**
     * @var FileDeleter
     */
    private $fileDeleter;

    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @var TraceableEventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * FileSupplierManager constructor.
     *
     * @param FileDeleter                       $fileDeleter
     * @param FileUploader                      $fileUploader
     * @param TraceableEventDispatcherInterface $eventDispatcher
     */
    public function __construct(FileDeleter $fileDeleter, FileUploader $fileUploader, TraceableEventDispatcherInterface $eventDispatcher)
    {

        $this->fileDeleter     = $fileDeleter;
        $this->fileUploader    = $fileUploader;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Deletes a supplier image from the image directory
     *
     * @param Supplier $supplier
     */
    public function deleteSupplierImage(Supplier $supplier)
    {
        $this->fileDeleter->delete($supplier->getLogo());
    }

    /**
     * Adds a supplier image to the image directory and
     * dispatches a new event with the new name of the image
     *
     * @param Supplier $supplier
     */
    public function addSupplierImage(Supplier $supplier)
    {
        $imageUploaded = $this->fileUploader->upload($supplier->getLogo());
        $supplier->setLogo($imageUploaded);
        $this->eventDispatcher->dispatch('supplier_image_uploaded', new ImageSupplierUploadedEvent($supplier));
    }

}