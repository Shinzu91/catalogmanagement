<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FileUploader
 *
 * @package FileManagerBundle\Services
 */
class FileUploader
{
    /**
     * @var
     */
    private $targetDir;

    /**
     * FileUploader constructor.
     *
     * @param $targetDir
     */
    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    /**
     * Generate a md5 new name and uploads the file
     *
     * @param UploadedFile $file
     *
     * @return string
     */
    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();

        $file->move($this->targetDir, $fileName);

        return $fileName;
    }

    /**
     * Gets the target directory
     *
     * @return mixed
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }
}