<?php

/**
 * FileManagerBundle for Symfony3
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle\Services;

/**
 * Class FileDeleter
 */
class FileDeleter
{
    /**
     * @var
     */
    private $targetDir;

    /**
     * FileDeleter constructor.
     *
     * @param $targetDir
     */
    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }


    /**
     * Deletes the image from the directory
     * 
     * @param String $image
     */
    public function delete(String $image)
    {
        unlink($this->targetDir . '/' . $image);
    }

    /**
     * Gets the target directory
     * 
     * @return mixed
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }
}