<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle\Event;

use CatalogBundle\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ImageProductUploadedEvent
 */
class ImageProductUploadedEvent extends Event
{
    /**
     * @var Product
     */
    private $product;

    /**
     * ImageProductUploadedEvent constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Gets a product updated
     *
     * @return Product
     */
    public function getUpdatedProduct()
    {
        return $this->product;
    }
}