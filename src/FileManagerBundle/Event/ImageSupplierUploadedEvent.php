<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle\Event;

use CatalogBundle\Entity\Supplier;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ImageSupplierUploadedEvent
 */
class ImageSupplierUploadedEvent extends Event
{
    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * ImageSupplierUploadedEvent constructor.
     *
     * @param Supplier $supplier
     */
    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * Gets a supplier updated
     *
     * @return Supplier
     */
    public function getUpdatedSupplier()
    {
        return $this->supplier;
    }
}