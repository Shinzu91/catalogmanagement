<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class FileManagerExtension, extending of extension
 */
class FileManagerExtension extends Extension
{
    /**
     * Loading the services of FileManagerBundle
     *
     * @param array            $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config/')
        );
        $loader->load('fileProductServices.yml');
        $loader->load('fileSupplierServices.yml');
    }
}