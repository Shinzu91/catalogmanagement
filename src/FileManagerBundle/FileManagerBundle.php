<?php

/**
 * FileManagerBundle for Symfony3
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * FileManagerBundle, an extension of Bundle.
 */
class FileManagerBundle extends Bundle
{
}