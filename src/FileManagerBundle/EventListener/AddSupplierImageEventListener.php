<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle\EventListener;

use FileManagerBundle\FileSupplierManager;

/**
 * Class AddSupplierImageEventListener
 */
class AddSupplierImageEventListener
{

    /**
     * @var FileSupplierManager
     */
    private $addedSupplierImage;

    /**
     * AddSupplierImageEventListener constructor.
     *
     * @param FileSupplierManager $addedSupplierImage
     */
    public function __construct(FileSupplierManager $addedSupplierImage)
    {

        $this->addedSupplierImage = $addedSupplierImage;
    }

    /**
     * Listens for adding a supplier image
     *
     * @param $event
     */
    public function addSupplierImage($event)
    {
        $this
            ->addedSupplierImage
            ->addSupplierImage(
                $event->getSupplier());
    }
}