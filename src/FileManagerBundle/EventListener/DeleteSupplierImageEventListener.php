<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle\EventListener;

use FileManagerBundle\FileSupplierManager;

/**
 * Class DeleteSupplierImageEventListener
 *
 * @package FileManagerBundle\EventListener
 */
class DeleteSupplierImageEventListener
{

    /**
     * @var FileSupplierManager
     */
    private $deletedSupplierImage;

    /**
     * DeleteSupplierImageEventListener constructor.
     *
     * @param FileSupplierManager $supplierFileManager
     */
    public function __construct(FileSupplierManager $supplierFileManager)
    {
        $this->deletedSupplierImage = $supplierFileManager;
    }

    /**
     * Listens for deleting a supplier image
     * 
     * @param $event
     */
    public function deleteSupplierImage($event)
    {
        $this
            ->deletedSupplierImage
            ->deleteSupplierImage(
                $event->getSupplierDeleted()
            );
    }
}