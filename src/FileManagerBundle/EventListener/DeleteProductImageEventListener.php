<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle\EventListener;

use FileManagerBundle\FileProductManager;

/**
 * Class DeleteProductImageEventListener
 */
class DeleteProductImageEventListener
{

    /**
     * @var FileProductManager
     */
    private $deletedProductImage;

    /**
     * DeleteProductImageEventListener constructor.
     *
     * @param FileProductManager $productFileManager
     */
    public function __construct(FileProductManager $productFileManager)
    {
        $this->deletedProductImage = $productFileManager;
    }

    /**
     * Listens for deleting a product image
     * 
     * @param $event
     */
    public function deleteProductImage($event)
    {
        $this
            ->deletedProductImage
            ->deleteProductImage(
                $event->getProductDeleted()
            );
    }
}