<?php

/**
 * FileManagerBundle for Symfony3
 *
 * @author Josep Blanch <blanch.royo@gmail.com>
 */

namespace FileManagerBundle\EventListener;

use FileManagerBundle\FileProductManager;

/**
 * Class AddProductImageEventListener
 */
class AddProductImageEventListener
{

    /**
     * @var FileProductManager
     */
    private $addedProductImage;

    /**
     * AddProductImageEventListener constructor.
     *
     * @param FileProductManager $addedProductImage
     */
    public function __construct(FileProductManager $addedProductImage)
    {

        $this->addedProductImage = $addedProductImage;
    }

    /**
     * Listens for adding a product image
     * 
     * @param $event
     */
    public function addProductImage($event)
    {
        $this
            ->addedProductImage
            ->addProductImage(
                $event->getProduct());
    }
}