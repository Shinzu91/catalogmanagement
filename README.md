# CatalogManagement

Este proyecto es un catálogo donde hay:

  - Proveedores
  - Productos

Estos proveedores y productos se pueden añadir, editar y eliminar. Para ello, han sido necesarios dos bundles:

  - **CatalogBundle**: se encarga de la lógica del site.
  - **FileManagerBundle**:  se encarga de gestionar las imagenes del catálogo mediante eventos.

# Como inicializar el proyecto
  - Clonar el repositorio
```sh
 git clone https://mmoreram@bitbucket.org/Shinzu91/catalogmanagement.git
```
  - Inicializar el proyecto
```sh
 php bin/console server:run
 php bin/console doctrine:database:create
 php bin/console doctrine:schema:update --force
```
  - Inicializar el Data Faker
```sh
  php bin/console doctrine:fixtures:load
```

- Ver el site
```sh
  http://localhost:8000/
```



